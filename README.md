# Radio Stations

The initial purpose of this script was to rid myself of using poorly functioning GUI radio applications and make a simplistic radio *app* for the command line. Latterly I converted it into a website to use with **iOS** devices.

## Radio CMD

![CMD](Radio_Terminal.jpg)

The main file is `radio.sh`. This script reads a text file containing radio stations in the format `$NAME | $ADDRESS`. Stations are played using **[mpv](https://mpv.io)**.

## Web Version
![Web Version](Radio_Website.jpg)

The [web version](https://lawrencechin.gitlab.io/Radio-Stations/radio.html) was intended to be a **PWA** of sorts that would be saved to the homescreen and run outside of **Safari**. Unfortunately, web apps can't utilize background audio so that isn't going to work for a radio app. I explored trying to extract meta data from the streams but couldn't find a satisfactory way to achieve this; perhaps for the future…

## Old Web Version

![Old web version](Radio_Website_Old.jpg)

Put up for posterity, looks quite pretty, might still work. View it [here](https://lawrencechin.gitlab.io/Radio-Stations/).

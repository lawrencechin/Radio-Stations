#!/bin/bash

rm -r public
mkdir -p public

# Convert radio stations list into markdown unordered list with links
echo "<ul id='stations'>" >> public/radio.md
cat radio_stations.txt | sed -E 's/^([^|]+)\|([^|]+)\|([^|]+)/<li class="\3 searchable">\[\1\]\(\2\) <span class="genre \3" data-genre="\3"\>\3\<\/span\><\/li>/g' >> public/radio.md
echo "</ul>" >> public/radio.md

pandoc -s \
    --css="style.css" \
    --template=template.html \
    public/radio.md \
    -o public/radio.html

cp main.js public
cp style.css public
cp -r favicons public
cp -r fonts public
cp -r imgs public
cp manifest.json public

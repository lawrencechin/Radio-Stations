# <span class="logo"></span>Radio Stations

<ul id="genreMenu"></ul>
<button id="searchBarBtn" class="activeBtn" title="Make Searchbar Active">🔎</button>
<button id="externalStreamBtn" class="inactiveBtn" title="Make External Stream Bar Active">🔊</button>
<input id="searchBar" name="searchBar" placeholder="Type to Search Stations">
<input id="externalStream" name="externalStream" placeholder="Enter Stream Address and Hit Enter Key">

<div id="audioContainer">
<audio src="#" controls="controls">
    <strong>Your browser doesn't support audio</strong>
</audio>
</div>

<!-- list generated from radio stations text file -->

/* Utilities */

// head :: [ a ] -> a
const head = xs => xs[ 0 ];

// prop :: String -> Object -> String
const prop = p => obj => obj[ p ];

// forEach :: ( a -> ()) -> [ a ] -> ()
const forEach = fn => xs => xs.forEach( fn );

/* Functions */

// createElement :: String -> ( Object ) -> Element 
const createElement = elem => elem_props => {
    const el = document.createElement( elem );
    for( const prop in elem_props )
        el[ prop ] = elem_props[ prop ];
    return el;
};

// getElements :: String -> [ a ]
const getElements = name => [ ...document.querySelectorAll( name )];

// getFirstElement :: String -> Object
const getFirstElement = name => head( getElements( name ));

// getUrl :: Object -> String
const getUrl = prop( "href" );

// getSource :: Object -> Object
const getSource = prop( "srcElement" );

// getClassList :: Object -> Object
const getClassList = prop( "classList" );

// addEvent :: String -> (a -> ()) -> Object -> ()
const addEvent = event => fn => obj => obj.addEventListener( event, fn );

// processMultipleEvents :: [ obj ] -> ()
const processMultipleEvents = props => {
    forEach(
        prop => {
            addEvent( 
                prop.evt 
            )( 
                prop.args.length > 0 
                    ? prop.handler( ...prop.args )
                    : prop.handler
            )(
                prop.elem
            )
        }
    )( props );
};

// changeSrc :: String -> Object -> ()
const changeSrc = newSrc => obj => obj.src = newSrc;

// addClass :: String -> Object -> ()
const addClass = name => obj => getClassList( obj ).add( name );

// removeClass :: String -> Object -> ()
const removeClass = name => obj => getClassList( obj ).remove( name );

// removeNamedClass = String -> Object
const removeNamedClass = name => forEach( removeClass( name ))( getElements( `.${ name }` ));

/* Generate genres */

// Generate list of genres from stations
const genreList = elems => {
    const genres = {};

    const addToGenres = elem => {
        const g = prop( "genre" )( elem.dataset );
        if( !genres[ g ]) genres[ g ] = 1;
    };

    forEach( addToGenres )( elems );

    return genres;
};

const createGenreMenu = ( menuElem, genresDict ) => {
    const filterSpan = createElement( "div" )({ innerText : "Filter by genre:" });
    const clearFilter = createElement( "li" )({ innerHTML : "<a href='#' class='genre' data-genre='0'>Clear</a>" });
    addClass( "clearFilter" )( clearFilter );
    addClass( "filtered" )( clearFilter );
    
    menuElem.appendChild( filterSpan );
    menuElem.appendChild( clearFilter );

    Object.keys( genresDict ).forEach( g => {
        const li = createElement( "li" )({ innerHTML : `<a href="#" class="genre" data-genre="${ g }">${ g }</a>` });
        addClass( g )( li );
        menuElem.appendChild( li );
    });
};

/* Event Handlers */
const stationEvent = ( audioElem, playBtn ) => e => {
    e.preventDefault();
    const source = getSource( e );
    const aElem = source.nodeName === "LI"
        ? head( source.getElementsByTagName( "a" ))
        : source.nodeName === "SPAN"
            ? head( source.parentElement.getElementsByTagName( "a" ))
            : source.nodeName === "A"
                ? source
                : false;
    const aParent = aElem.parentElement;
    const url = getUrl( aElem );
    const stationName = aElem.innerText;

    if( !url ) return;

    removeNamedClass( "selected" );
    changeSrc( url )( audioElem );
    playBtn.click();
    addClass( "selected" )( aParent );
    npText( stationName );
};

const audioErrEvent = () => {
    const station = getFirstElement( ".selected" );
    if( station ){
        addClass( "stationErr" )( station );
        removeClass( "selected" )( station );
        npText( "Station Unavailable" );
    }
};

const searchBtnEvent = e => {
    const btn = e.target;
    addClass( "activeBtn" )( btn );
    removeClass( "activeBtn" )( btn.nextElementSibling );
};

const externalBtnEvent = e => {
    const btn = e.target;
    addClass( "activeBtn" )( btn );
    removeClass( "activeBtn" )( btn.previousElementSibling );
};

// evt: input, elems searchBarElem & stations
const searchBarEvent = elemsToSearch => e => {
    const searchTerm = new RegExp( e.target.value, "i" );
    forEach( 
        elem => {
            const parent = elem.parentElement;
            if( getClassList( parent ).contains( "searchable" ) && searchTerm.test( elem.innerText ))
                removeClass( "hide" )( parent )
            else
                addClass( "hide" )( parent )
        }
    )( elemsToSearch );
};

// event: keyup
const playExternalStreamEvent = ( audioElem, playBtn ) => e => {
    if( e.keyCode !== 13 ) return;
    const address = e.target.value;
    changeSrc( address )( audioElem );
    playBtn.click();
    removeNamedClass( "selected" );
    npText( "External Stream" );
};

// event: change on slider
const volumeSliderEvent = audioElem => e => {
    audioElem.volume = e.target.value / 100;
};

const playBtnEvent = audioElem => e => {
    removeClass( "activeBtn" )( e.target.nextElementSibling );

    audioElem.play().catch( err => err ).then( err => {
        if( err ) removeClass( "activeBtn" )( e.target );
        else addClass( "activeBtn" )( e.target );
    });
};

const pauseBtnEvent = audioElem => e => {
    removeClass( "activeBtn" )( e.target.previousElementSibling );

    if( !audioElem.paused ){
        addClass( "activeBtn" )( e.target );
        audioElem.pause();
    } else
        removeClass( "activeBtn" )( e.target );
};

const genreEvent = containerToFilter => e => {
    e.preventDefault();
    const target = getSource( e );
    const genre = prop( "genre" )( target.dataset );
    const currentlyFiltered = getClassList( target.parentElement ).contains( "filtered" );
    if( !genre ) return;

    removeNamedClass( "filtered" );
    if( currentlyFiltered )
        getFirstElement( ".clearFilter a" ).click();
    else {
        addClass( "filtered" )( target.parentElement );
        forEach( elem => {
            if( genre === "0" ){
                removeClass( "hide" )( elem.parentElement );
                addClass( "searchable" )( elem.parentElement );
            } else {
                if( prop( "genre" )( elem.dataset ) === genre ){
                    removeClass( "hide" )( elem.parentElement )
                    addClass( "searchable" )( elem.parentElement );
                } else {
                    addClass( "hide" )( elem.parentElement );
                    removeClass( "searchable" )( elem.parentElement )
                }
            }
        })( containerToFilter );
    }
};

/* App */
/* DOM Elements */
const genreMenu = getFirstElement( "#genreMenu" );
const stationGenres = getElements( "#stations span" );
const nowPlaying = getFirstElement( "#nowPlaying" );
const searchBtn = getFirstElement( "#searchBarBtn" );
const externalBtn = getFirstElement( "#externalStreamBtn" );
const stationLinks = getElements( "#stations a" );
const searchBar = getFirstElement( "#searchBar" );
const externalBar = getFirstElement( "#externalStream" );
const audioElem = getFirstElement( "audio" );
const playBtn = getFirstElement( "#playBtn" );
const stationsCont = getFirstElement( "#stations" );
const volumeSlider = getFirstElement( "#volumeControl" );
const pauseBtn = getFirstElement( "#pauseBtn" );

/* Events Array */
const evtArr = [
    { evt: "click", handler: searchBtnEvent, args: [], elem: searchBtn },
    { evt: "click", handler: externalBtnEvent, args: [], elem: externalBtn },
    { evt: "input", handler: searchBarEvent, args: [ stationLinks ], elem: searchBar },
    { evt: "keyup", handler: playExternalStreamEvent, args: [ audioElem, playBtn ], elem: externalBar },
    { evt: "click", handler: stationEvent, args: [ audioElem, playBtn ], elem: stationsCont },
    { evt: "click", handler: genreEvent, args: [ stationGenres ], elem: genreMenu },
    { evt: "change", handler: volumeSliderEvent, args: [ audioElem ], elem: volumeSlider },
    { evt: "click", handler: playBtnEvent, args: [ audioElem ], elem: playBtn },
    { evt: "click", handler: pauseBtnEvent, args: [ audioElem ], elem: pauseBtn },
    { evt: "error", handler: audioErrEvent, args: [], elem: audioElem }
];

createGenreMenu( genreMenu, genreList( stationGenres ));

const nowPlayingText = elem => str => { elem.innerText = str; };
const npText = nowPlayingText( nowPlaying );

/* Attaching handlers to events */
processMultipleEvents( evtArr );

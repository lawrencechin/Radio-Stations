#!/bin/bash

## Variables
# colours
nc="\033[0m"
red="\033[0;31m"
green="\033[0;32m"
bblue="\033[1;34m"
on_green="\033[42m"
# file containing radio stations
file="/radio_stations.txt"
# script dir
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# array holding station address referenced later
radio_array=()
# incrementing variable
arr_inc=1
# string containing list
str=""

## Functions
# Read and loop over file
read_and_loop() {
    IFS="|"
    while read name address genre; do
        trunc_name="$( echo -e $name | cut -c 1-100 )"
        station_name="$arr_inc: $trunc_name"
        if ! (( $arr_inc % 2 )); then
            str+="${bblue}$station_name${nc}\n"
        else
            str+="${red}$station_name${nc}\n"
        fi
        arr_inc=$(( $arr_inc + 1 ))
        radio_array[$arr_inc]=$address
    done < $DIR$file 
}
# Get user response
user_response() {
     echo -e "Type in the station number to listen"
     echo -e "Type ${on_green} p ${nc} to print stations or ${on_green} q ${nc} to exit"
     echo -e "Station ${green} >${nc} "
    read  REPLY
    if [[ "$REPLY" = "q" ]]; then
        exit
    elif [[ "$REPLY" = "p" ]]; then
        arr_inc=1
        print_stations
        user_response
    else
        if [[ "${radio_array[$(( REPLY + 1 ))]}" = "" ]]; then
             echo -e "Please select a valid station"
             echo -e "$REPLY"
            user_response
        else
            controls
            play_stream "${radio_array[$(( REPLY + 1 ))]}"
        fi
    fi
}
# Play stream
function play_stream {
    mpv "$1"
    print_stations
    user_response
}
# Print list
print_stations() {
     echo -e $str | column -c `tput cols` 
}
# Controls
controls() {
    separator="${red}*${nc}"
     echo -e "Controls : ${on_green} p | space ${nc} to pause $separator ${on_green} q ${nc} to stop playback $separator ${on_green} m ${nc} to mute $separator ${on_green} 9 | 0 ${nc} to change the volume" 
}

read_and_loop
print_stations
user_response
